# Static Deployment 
This repo was created as an exercise in setting up my first CI/CD Pipeline in GitLab to deploy a static webpage (`index.html`) to an S3 Bucket.

## Stages of my Pipeline
*(As of 2022-02-28)*

Stages:
 - Static Analysis
    - Runs pylint 
- Test
    - Runs pytest
- Deploy
    - Deploys to my AWS S3 Bucket


## Steps I took which are not visible in this repo:

### Setting up an S3 Bucket
* Properties altered to allow static website hosting
* Permissions changes to **Public** Access
* Used a [template Bucket Policy](https://docs.aws.amazon.com/AmazonS3/latest/userguide/example-bucket-policies.html) from AWS: "Granting read-only permission to an anonymous user" 

### GitLab Steps
Via Settings > CI/CD > Variables, set the following variables:
* AWS_DEFAULT_REGION
* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* S3_BUCKET - with the value being the name of my S3 Bucket
This last variable is used in my `.gitlab-ci.yml` file so that I'm not exposing the name of my S3 Bucket.

### Lambda with Query Params
This lambda takes a query param "colour" and can be used to change the background of the html file (or font, or container, anything with a changeable colour)
* Create new lambda with AWS with the following code, using Python:
```
def lambda_handler(event, context):
    print(event['queryStringParameters']['colour'])
    
    colour = event['queryStringParameters']['colour']

    response = {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Origin": "*",
        },
        "body": colour
    }

    return response
```
* Create/Open existing API Gateway via AWS
* Add new GET method using new lambda - making sure 'Use Lambda Proxy integration' is ticked
* Under **Method Request** add your param in URL Query String Parameters
* Get your gateway URL from your lambda*
* View the 'index.html' file to view how this lambda is then used within the script via `queryFunction()`

You'll know it's been set up properly if when you follow the URL and add `?colour=blue` at the end, the page loads with just the word 'blue' on it.
