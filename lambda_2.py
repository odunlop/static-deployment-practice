def lambda_handler(event, context):
    print(event['queryStringParameters']['colour'])
    
    colour = event['queryStringParameters']['colour']

    response = {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Origin": "*",
        },
        "body": colour
    }

    return response